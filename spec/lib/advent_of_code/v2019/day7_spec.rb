require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day7 do
  it "returns the max thruster signal" do
    # This example is taken from the puzzle prompt, updated for part 2.
    fixture = "#{RSPEC_ROOT}/support/fixtures/2019/day7_input_example.txt"
    expect(described_class.run(fixture)).to eq(139629729)
  end
end
