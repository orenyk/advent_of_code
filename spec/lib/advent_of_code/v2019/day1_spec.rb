require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day1 do
  describe ".run" do
    it "takes a file containing masses and returns the total fuel required" do
      # the file in spec/support/fixtures contains the four masses listed below:
      # 12, 14, 1969, and 100,756. We then need to iteratively calculate the
      # fuel required for the added fuel mass until we hit zero. For this simple
      # example, the value is 51,331.
      fixture = "#{RSPEC_ROOT}/support/fixtures/day1_input_example.txt"
      expect(described_class.run(fixture)).to eq(51_316)
    end
  end

  describe "#calculate_fuel" do
    # Note that these take into account the total fuel calculations
    TEST_CASES = {
      2 => 0,
      12 => 2,
      14 => 2,
      1_969 => 966,
      100_756 => 50_346
    }.freeze

    TEST_CASES.each do |input, output|
      it "returns #{output} for #{input}" do
        expect(described_class.new(input).calculate_fuel).to eq(output)
      end
    end
  end
end
