require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day3 do
  describe ".run" do
    it "returns the distance of the closest intersection for two wires" do
      # This example is taken from the puzzle prompt. Updated for Part 2.
      fixture = "#{RSPEC_ROOT}/support/fixtures/2019/day3_input_example.txt"
      expect(described_class.run(fixture)).to eq(610)
    end
  end
end
