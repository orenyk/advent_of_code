require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day6 do
  it "returns the minimum number of orbital transfers" do
    # This example is taken from the puzzle prompt, updated for part 2.
    fixture = "#{RSPEC_ROOT}/support/fixtures/2019/day6_input_example.txt"
    expect(described_class.run(fixture)).to eq(4)
  end
end
