require "spec_helper"

RSpec.describe AdventOfCode::V2019::Computer do
  describe "#run_program" do
    EXAMPLES = {
      [1,0,0,0,99] => [2,0,0,0,99],
      [2,3,0,3,99] => [2,3,0,6,99],
      [2,4,4,5,99,0] => [2,4,4,5,99,9801],
      [1,1,1,4,99,5,6,0,99] => [30,1,1,4,2,5,6,0,99],
      [1,9,10,3,2,3,11,0,99,30,40,50] => [3500,9,10,70,2,3,11,0,99,30,40,50]
    }.freeze

    EXAMPLES.each do |input, final|
      it "returns #{final} for #{input}" do
        outputs = described_class.new(input, []).run_program
        expect(outputs[:memory]).to eq(final)
      end
    end
  end
end
