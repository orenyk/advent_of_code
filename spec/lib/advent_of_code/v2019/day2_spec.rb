require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day2 do
  describe ".run" do
    it "returns the noun and verb for a given input file and output value" do
      # This example is taken from part 1, the first working case where the first
      # address ends up being 1 is [0, 6] once we add parameter modes in day 5.
      fixture = "#{RSPEC_ROOT}/support/fixtures/day2_input_example.txt"
      expect(described_class.run(fixture, 1)).to eq([0, 6])
    end
  end
end
