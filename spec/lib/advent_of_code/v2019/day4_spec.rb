require "spec_helper"

RSpec.describe AdventOfCode::V2019::Day4 do
  describe ".run" do
    it "returns the number of valid passwords in a range" do
      # Trivial example, updated for Part 2.
      expect(described_class.run(111_111, 111_122)).to eq(1)
    end
  end
end
