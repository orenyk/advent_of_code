require "advent_of_code/v2019"
require "advent_of_code/v2020"
require "advent_of_code/v2021"
require "advent_of_code/v2022"
require "advent_of_code/version"

module AdventOfCode
  class Error < StandardError; end
  # Your code goes here...
end
