module AdventOfCode
  module V2020
    class Day13
      def self.run(file, contest = false)
        new(file).run(contest)
      end

      def initialize(file)
        lines = File.readlines(file)
        @timestamp = lines[0].strip.to_i
        @buses = parse_buses(lines[1].strip)
      end

      def run(contest)
        if contest
          find_contest_timestamp
        else
          find_earliest_bus
        end
      end

      private

      attr_reader :timestamp, :buses

      def parse_buses(line)
        line.split(',').each_with_index.filter_map { |s, i| [s.to_i, i] unless s == 'x' }
      end

      def find_earliest_bus
        bus_times = buses.map do |bus|
          [bus[0], bus[0] - (timestamp % bus[0])]
        end.to_h

        first_bus = bus_times.invert[bus_times.values.min]
        first_bus * bus_times[first_bus]
      end

      def find_contest_timestamp
        bus_hash = buses.to_h
        first_bus = buses.first[0]
        bus_hash.delete(first_bus)

        # we need to go through each bus and iteratively build out the full
        # equation for calculating the timestamp. For each bus, we will
        # determine the constant and coefficient for each "level" of the
        # equation.
        #
        # Given the initial bus b0 and bus 1 (value b1), there is some set of
        # values k1 and c1 such that b0 * (k1 + c1 * b1) will always result in
        # bus 1 arriving at the correct offset (k1 must be searched for by
        # searching for the value where (k1 * b0) % b1 is equal to the offset
        # defined by the puzzle input).
        #
        # Once we have solved for bus 1, we now can define c1 (the multiple of
        # b1 that is added to constant k1) such that bus 2 is also at the
        # correct offset. This follows a similar process: first - find the same
        # "top level" offset for b2 where (k2 * b0) % b2 fits the puzzle input.
        # Then, we can set k1 + c1 * b1 = k2 + c2 * b2 (e.g. the timestamp must
        # be identical) and find then "next level" offset to calculate c1.
        # We rewrite the equation as c1 = k2' + c2' * b2 and find k2' the same
        # way we found k1 and k2, where we're looking for k2a such that
        # (k2' * b1) % b2 = -(k1 - k2). Thus, we can rewrite our timestamp
        # equation as b0 * (k1 + k2' * b1) and buses 1 & 2 will always be
        # satisfied.
        #
        # From here, we rinse and repeat until we have built an equation of the
        # form t = k0 + (k1 + (k2' + (k3'' + ...) * b2) * b1) * b0

        # store the equation as a set of coefficients for each bus going through
        # the chain, we'll go recursively from the last set of coefficients for 
        # each. Each set of equations will be [bi, ki]
        equations = find_equations(bus_hash, [[[first_bus, 0]]])

        calculate_timestamp(equations)
      end

      def find_equations(bus_hash, equations)
        return equations if bus_hash.empty?

        eq_count = equations.length
        bus, offset = bus_hash.shift

        new_equations = equations[0..(eq_count - 1)].each_with_object([]) do |eq_array, eqs|
          eq = if eqs.empty?
                 find_equation(eq_array.last, [bus, -1 * offset])
               else
                 find_equation(eq_array.last, eqs.last)
               end
          eqs << eq
          eqs
        end

        equations << new_equations
        find_equations(bus_hash, equations)
      end

      def find_equation(other_eq, new_eq)
        [new_eq[0], find_coeff_for_offset(new_eq[0], other_eq[0], other_eq[1] - new_eq[1])]
      end

      def find_coeff_for_offset(bus, bus_to_multiply, offset)
        mod = if offset.positive?
                bus - offset % bus
              else
                (offset.abs % bus)
              end
        n = 1
        loop do
          break if n * bus_to_multiply % bus == mod
          n += 1
        end
        n
      end

      def calculate_timestamp(equations)
        equations.reverse.reduce(1) do |acc, eq_array|
          if acc == 1
            acc * eq_array.last[1]
          else
            acc * eq_array.last[0] + eq_array.last[1]
          end
        end
      end
    end
  end
end
