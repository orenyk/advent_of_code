module AdventOfCode
  module V2020
    class Day14
      def self.run(file, version = '1')
        new(file, version.to_i).run
      end

      def initialize(file, version)
        @version = version
        @commands = parse_commands(file)
        @memory = {}
        @bitmask = {}
      end

      def run
        @memory = {}
        @bitmask = {}

        commands.each do |command|
          case command[:cmd]
          when 'mem'
            if version == 1
              write_masked_to_mem(command[:addr], command[:val])
            elsif version == 2
              write_to_masked_mem(command[:addr], command[:val])
            end
          when 'mask'
            @bitmask = parsed_bitmask(command[:val])
          end
        end

        memory.values.sum
      end

      private

      COMMAND_REGEX = /^(?<cmd>mask|mem)(\[(?<addr>\d+)\])? = (?<val>\d+|[X01]{36})$/

      attr_reader :version, :commands, :memory, :bitmask

      def parse_commands(file)
        File.readlines(file).map do |line|
          match = COMMAND_REGEX.match(line)

          case match[:cmd]
          when 'mem'
            { cmd: 'mem', addr: match[:addr].to_i, val: match[:val].to_i }
          when 'mask'
            { cmd: 'mask', val: match[:val] }
          end
        end
      end

      def parsed_bitmask(mask_str)
        if version == 1
          parsed_mask_v1(mask_str)
        elsif version == 2
          parsed_mask_v2(mask_str)
        end
      end

      def parsed_mask_v1(mask_str)
        {
          and: mask_str.gsub(/[01]/, '1').gsub(/X/, '0').to_i(2),
          or: mask_str.gsub(/X/, '0').to_i(2)
        }
      end

      def write_masked_to_mem(addr, value)
        @memory[addr] = masked_val(value)
      end

      def masked_val(value, mask = bitmask)
        (~mask[:and] & value) | mask[:or]
      end

      def parsed_mask_v2(mask_str)
        num_xs = mask_str.count('X')
        (0..(2 ** num_xs - 1)).map do |floating_value|
          chars = floating_value.to_s(2).rjust(num_xs, '0').chars
          {
            and: mask_str.gsub(/X/, '1').to_i(2),
            or: mask_with_floating_bits(mask_str, chars)
          }
        end
      end

      def mask_with_floating_bits(mask_str, chars)
        str = mask_str.clone
        chars.each { |c| str.sub!(/X/, c) }
        str.to_i(2)
      end

      def write_to_masked_mem(addr, value)
        bitmask.each do |mask|
          @memory[masked_val(addr, mask)] = value
        end
      end
    end
  end
end
