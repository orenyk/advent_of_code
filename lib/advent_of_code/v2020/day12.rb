module AdventOfCode
  module V2020
    class Day12
      def self.run(file, waypoint = false)
        new(file).run(waypoint)
      end

      def initialize(file)
        @instructions = parse_instructions(file)
        @direction = DIRS['E'][:angle]
        @x = 0
        @y = 0
        @waypoint_x = 10
        @waypoint_y = 1
      end

      def run(waypoint)
        # reset
        @direction = DIRS['E'][:angle]
        @x = 0
        @y = 0
        @waypoint_x = 10
        @waypoint_y = 1

        if waypoint
          run_instructions_waypoint
        else
          run_instructions_ship
        end

        manhattan_distance
      end

      private

      DIRS = {
        'E' => { angle: 0, vector: [1, 0] },
        'N' => { angle: 90, vector: [0, 1] },
        'W' => { angle: 180, vector: [-1, 0] },
        'S' => { angle: 270, vector: [0, -1] }
      }.freeze

      ANGLES = {
        0 => 'E',
        90 => 'N',
        180 => 'W',
        270 => 'S'
      }.freeze

      attr_reader :instructions, :direction, :x, :y, :waypoint_x, :waypoint_y

      def parse_instructions(file)
        File.readlines(file).map do |l|
          match = /^(?<cmd>[NSEWLRF])(?<val>\d+)$/.match(l.strip)
          { cmd: match[:cmd], val: match[:val].to_i }
        end
      end

      def run_instructions_ship
        instructions.each do |instruction|
          if DIRS.keys.include? instruction[:cmd]
            move_ship(instruction[:val], instruction[:cmd])
          elsif instruction[:cmd] == 'L'
            turn_ship(instruction[:val])
          elsif instruction[:cmd] == 'R'
            turn_ship(-1 * instruction[:val])
          elsif instruction[:cmd] == 'F'
            move_ship(instruction[:val])
          end
        end
      end

      def move_ship(val, dir = 'F')
        vector = if dir == 'F'
                   DIRS[ANGLES[direction]][:vector]
                 else
                   DIRS[dir][:vector]
                 end
        @x += val * vector[0]
        @y += val * vector[1]
      end

      def turn_ship(angle)
        @direction += angle
        @direction -= 360 if direction >= 360
        @direction += 360 if direction.negative?
      end

      def manhattan_distance
        x.abs + y.abs
      end

      def run_instructions_waypoint
        instructions.each do |instruction|
          if DIRS.keys.include? instruction[:cmd]
            move_waypoint(instruction[:val], instruction[:cmd])
          elsif instruction[:cmd] == 'L'
            rotate_waypoint(instruction[:val])
          elsif instruction[:cmd] == 'R'
            rotate_waypoint(-1 * instruction[:val])
          elsif instruction[:cmd] == 'F'
            move_towards_waypoint(instruction[:val])
          end
        end
      end

      def move_waypoint(val, dir)
        vector = DIRS[dir][:vector]
        @waypoint_x += val * vector[0]
        @waypoint_y += val * vector[1]
      end

      def rotate_waypoint(angle)
        angle = 360 + angle if angle.negative?
        current_waypoint = [waypoint_x, waypoint_y]

        case angle
        when 0
        when 90
          @waypoint_x = -1 * current_waypoint[1]
          @waypoint_y = current_waypoint[0]
        when 180
          @waypoint_x *= -1
          @waypoint_y *= -1
        when 270
          @waypoint_x = current_waypoint[1]
          @waypoint_y = -1 * current_waypoint[0]
        end
      end

      def move_towards_waypoint(val)
        @x += val * @waypoint_x
        @y += val * @waypoint_y
      end
    end
  end
end
