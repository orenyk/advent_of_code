module AdventOfCode
  module V2020
    class Day7
      def self.run(file, count_children = false)
        new(file).run(count_children)
      end

      def initialize(file)
        @parent_rules = Hash.new { [] }
        @child_rules = {}
        parse_rules(file)
      end

      def run(count_children)
        if count_children
          count_children('shiny gold')
        else
          count_parents('shiny gold')
        end
      end

      private

      attr_reader :parent_rules, :child_rules

      def parse_rules(file)
        File.readlines(file).each { |r| parse_rule(r.strip) }
      end

      def parse_rule(rule)
        parent = /^(?<color>[a-z ]+) bags contain/.match(rule)[:color]
        children = rule.scan(/(?<count>\d+) (?<color>[a-z ]+) bags?/).map(&:reverse).to_h.transform_values(&:to_i)

        @child_rules[parent] = children

        return if children.empty?

        children.each { |child, _| @parent_rules[child] += [parent] }
      end

      def count_parents(color)
        find_parents(color, []).uniq.count
      end

      # go through each parent, build array of all parents until you reach
      # top-level colors
      def find_parents(color, parents)
        return parents if parent_rules[color].empty?

        parent_rules[color].reduce([]) { |array, parent| array + find_parents(parent, parents + [parent]) }
      end

      def count_children(color)
        return 0 if child_rules[color].empty?

        child_rules[color].reduce(0) { |acc, (child, count)| acc + count * (count_children(child) + 1) }
      end
    end
  end
end
