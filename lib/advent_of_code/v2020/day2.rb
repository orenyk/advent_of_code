module AdventOfCode
  module V2020
    class Day2
      def self.run(file, type)
        new(file).run(type)
      end

      def initialize(file)
        @file = file
        @num_valid = 0
      end

      def run(type)
        File.open(file) do |f|
          f.each_line do |line|
            match = REGEX.match(line)
            @num_valid += 1 if send(validation_method(type), match)
          end
        end

        num_valid
      end

      private

      REGEX = /^(?<n1>\d+)-(?<n2>\d+) (?<char>[a-z]): (?<pass>[a-z]+)$/

      attr_reader :file, :num_valid

      def validation_method(type)
        "valid_password_by_#{type}?".to_sym
      end

      def valid_password_by_count?(match)
        min = match[:n1].to_i
        max = match[:n2].to_i
        count = match[:pass].chars.count { |c| c == match[:char] }

        count >= min && count <= max
      end

      def valid_password_by_pos?(match)
        pos1 = match[:n1].to_i - 1
        pos2 = match[:n2].to_i - 1
        pass = match[:pass].chars
        chars_to_check = [pass[pos1], pass[pos2]]

        chars_to_check.include?(match[:char]) && chars_to_check.uniq.length == 2
      end
    end
  end
end
