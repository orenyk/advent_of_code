module AdventOfCode
  module V2020
    class Day6
      def self.run(file, check_for_all = false)
        new(file).run(check_for_all)
      end

      def initialize(file)
        @groups = parse_groups(file)
      end

      def run(check_for_all)
        groups.reduce(0) do |count, group|
          if check_for_all
            count += count_all(group)
          else
            count += count_any(group)
          end
        end
      end

      private

      attr_reader :groups

      def parse_groups(file)
        File.read(file).split("\n\n").map { |g| g.split("\n") }
      end

      def count_all(group)
        group.map { |g| g.chars }.reduce(('a'..'z').to_a, &:&).length
      end

      def count_any(group)
        group.join.chars.uniq.count
      end
    end
  end
end
