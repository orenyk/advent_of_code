module AdventOfCode
  module V2020
    class Day5
      def self.run(file, find_mine = false)
        new(file).run(find_mine)
      end

      def initialize(file)
        @passes = parse_passes(file)
      end

      def run(find_mine)
        return passes.keys.max unless find_mine

        valid_rows = passes.select { |_, p| !FRONT_BACK_REGEX.match(p) }.keys
        (valid_rows.min..valid_rows.max).to_a - valid_rows
      end

      private

      FRONT_BACK_REGEX = /^(F{7}|B{7})[LR]{3}$/

      attr_reader :passes

      def parse_passes(file)
        File.readlines(file).map { |p| p.strip!; [seat_id(p), p] }.to_h
      end

      def seat_id(pass)
        Integer("0b#{pass.gsub(/[FL]/, '0').gsub(/[BR]/, '1')}")
      end
    end
  end
end
