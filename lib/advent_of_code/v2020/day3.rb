require 'json'

module AdventOfCode
  module V2020
    class Day3
      # Public interface for utility class
      #
      # @attr file [String] the input file to parse
      # @attr slopes [String] a string representation of nested arrays
      #   representing the different slopes to check, e.g. "[[1, 2], [1, 3]]"
      def self.run(file, slopes)
        parsed_slopes = JSON.parse(slopes).map { |s| s.map(&:to_i) }
        new(file).run(parsed_slopes)
      end

      def initialize(file)
        @grid = parse_file(file)
        @n_rows = grid.length
        @n_cols = grid.first.length
        @num_trees = 0
        @tree_product = 1
        @x = 0
        @y = 0
      end

      # Calculate the product of all of the tree counts for each of the slopes
      # passed in
      #
      # @attr slopes [Array<Array<Integer>>] an array of two-element arrays
      #   representing slopes to follow. The first element is the increment
      #   right and the second element is the increment down.
      # @return [Integer] the product of the tree counts of all of the slopes
      def run(slopes)
        slopes.each do |slope|
          @tree_product *= count_trees_for_slope(*slope)
        end

        tree_product
      end

      def count_trees_for_slope(right, down)
        @x = 0
        @y = 0
        @num_trees = 0

        count_trees(right, down)
      end

      private

      attr_reader :grid, :num_trees, :tree_product, :n_rows, :n_cols, :x, :y

      def parse_file(file)
        File.readlines(file).map { |row| row.strip.chars }
      end

      def count_trees(right, down)
        while y < n_rows
          current_cell = grid[y][x]
          @num_trees += 1 if current_cell == '#'
          @y += down
          increment_x(right)
        end

        num_trees
      end

      def increment_x(right)
        @x += right

        return if x < n_cols

        @x -= n_cols
      end
    end
  end
end
