require 'json'

module AdventOfCode
  module V2020
    class Day18
      def self.run(file, order = false)
        new(file).run(order)
      end

      def initialize(file)
        @eqs = parse_eqs(file)
      end

      def run(order)
        equations = if order
                      eqs.map { |eq| gather_addition(eq) }
                    else
                      eqs
                    end
        equations.reduce(0) { |acc, eq| acc + evaluate_eq(eq) }
      end

      private

      NUM_REGEX = /(?<open>\(*)(?<num>\d+)(?<close>\)*)/

      attr_reader :eqs

      def parse_eqs(file)
        File.readlines(file).map do |r|
          JSON.parse("["+r.strip.gsub(' ', ',').gsub('(', '[ ').gsub(')', ' ]').gsub(/(?<c>[\*\+])/, '"\k<c>"')+"]")
        end
      end

      def evaluate_eq(eq, idx = 0, acc = 0, symbol = '+')
        # if we went past the end, return the accumulator
        return acc if symbol.nil?

        # if the next element is an array, calculate it as a separate equation
        if eq[idx].is_a? Array
          case symbol
          when '+'
            return evaluate_eq(eq, idx + 2, acc + evaluate_eq(eq[idx], 0, 0), eq[idx + 1])
          when '*'
            return evaluate_eq(eq, idx + 2, acc * evaluate_eq(eq[idx], 0, 0), eq[idx + 1])
          end
        end

        # if we're at the last element, use the last known symbol
        if idx == eq.length - 1
          case symbol
          when '+'
            return acc + eq[idx]
          when '*'
            return acc * eq[idx]
          end
        end

        # otherwise, normal math
        case symbol
        when '+'
          evaluate_eq(eq, idx + 2, acc + eq[idx], eq[idx + 1])
        when '*'
          evaluate_eq(eq, idx + 2, acc * eq[idx], eq[idx + 1])
        end
      end

      def gather_addition(eq, idx = 0)
        # if we're looking at an array, recurse!
        eq[idx] = gather_addition(eq[idx]) if eq[idx].is_a? Array

        # when we get to the end, finish
        if idx == eq.length
          return eq
        end

        # if we find a plus in an array that includes asterisks, group that
        # addition to prioritize it and recurse over the next item if it is an
        # Array. Note that since we delete two elements from the original array
        # that we repeat the same index.
        if eq[idx] == '+' && eq.count('*').positive?
          eq[idx - 1] = if eq[idx + 1].is_a? Array
                          [eq[idx - 1], '+', gather_addition(eq[idx + 1])]
                        else
                          [eq[idx - 1], '+', eq[idx + 1]]
                        end
          eq.delete_at(idx)
          eq.delete_at(idx)
          return gather_addition(eq, idx)
        end

        # otherwise, move forward
        gather_addition(eq, idx + 1)
      end
    end
  end
end
