module AdventOfCode
  module V2020
    class Day8
      def self.run(file, fixing = false)
        new(file).run(fixing)
      end

      def initialize(file)
        @commands = parse_commands(file)
        @accumulator = 0
        @current_pos = 0
        @command_counts = Hash.new { 0 }
      end

      def run(fixing)
        if fixing
          fix_program
        else
          run_until_loop_or_end
        end
        accumulator
      end

      private

      COMMAND_REGEX = /^(?<cmd>nop|acc|jmp) (?<val>[+-]\d+)$/

      attr_reader :commands, :accumulator, :current_pos, :command_counts

      def parse_commands(file)
        File.readlines(file).map do |cmd_str|
          match = COMMAND_REGEX.match(cmd_str)
          { cmd: match[:cmd], val: match[:val].to_i }
        end
      end

      def run_until_loop_or_end(cmds = commands)
        # start from scratch
        @accumulator = 0
        @current_pos = 0
        @command_counts = Hash.new { 0 }

        while true
          # check for end condition or loop
          return if current_pos == cmds.length || command_counts[current_pos] == 1

          current_command = cmds[current_pos]
          @command_counts[current_pos] += 1

          case current_command[:cmd]
          when 'nop'
            @current_pos += 1
          when 'acc'
            @accumulator += current_command[:val]
            @current_pos += 1
          when 'jmp'
            @current_pos += current_command[:val]
          end
        end
      end

      def fix_program
        lines_to_check = commands.each_with_index.filter_map do |cmd, i|
          next i if %w[nop jmp].include? cmd[:cmd]
        end

        lines_to_check.each do |line_no|
          cmds_clone = commands.clone
          flip_line(cmds_clone, line_no)
          run_until_loop_or_end(cmds_clone)

          break if current_pos == commands.length
        end
      end

      def flip_line(cmds, line_no)
        current_command = cmds[line_no][:cmd]
        new_command = if current_command == 'nop'
                        'jmp'
                      elsif current_command == 'jmp'
                        'nop'
                      else
                        current_command
                      end
        cmds[line_no] = { cmd: new_command, val: cmds[line_no][:val] }
      end
    end
  end
end
