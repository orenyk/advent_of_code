module AdventOfCode
  module V2020
    class Day11
      def self.run(file, adjacent = true)
        adjacent = adjacent.to_s.downcase == 'true'
        new(file, adjacent).run
      end

      def initialize(file, adjacent)
        @grid = parse_grid(file, adjacent)
        @adjacent = adjacent
      end

      def run
        if adjacent
          grid.adjacent_run_until_stable!
        else
          grid.visible_run_until_stable!
        end
        grid.num_occupied
      end

      private

      attr_reader :grid, :adjacent

      def parse_grid(file, adjacent)
        Grid.new(File.readlines(file).map { |l| l.strip.chars }, adjacent)
      end

      class Grid
        def initialize(cell_array, adjacent)
          @seats = generate_grid(cell_array, adjacent)
        end

        def adjacent_run_until_stable!
          loop do
            changed = false
            last_seats = seats.clone.transform_values { |s| s.clone }

            seats.transform_values do |seat|
              if seat.occupied? && more_than_x_neighbors_occupied?(last_seats, seat.neighbors, 5)
                changed = true
                seat.empty!
              elsif seat.unoccupied? && no_occupied_neighbors?(last_seats, seat.neighbors)
                changed = true
                seat.occupy!
              end
            end

            break unless changed
          end
        end

        def visible_run_until_stable!
          loop do
            changed = false
            last_seats = seats.clone.transform_values { |s| s.clone }

            seats.transform_values do |seat|
              if seat.occupied? && more_than_x_neighbors_occupied?(last_seats, seat.neighbors, 5)
                changed = true
                seat.empty!
              elsif seat.unoccupied? && no_occupied_neighbors?(last_seats, seat.neighbors)
                changed = true
                seat.occupy!
              end
            end

            break unless changed
          end
        end

        def num_occupied
          seats.count { |_, seat| seat.occupied? }
        end

        private

        DIRS = [
          [1, 0],
          [1, 1],
          [0, 1],
          [-1, 1],
          [-1, 0],
          [-1, -1],
          [0, -1],
          [1, -1]
        ]

        attr_reader :seats

        def generate_grid(cell_array, adjacent)
          n_rows = cell_array.length
          n_cols = cell_array.first.length

          Hash.new.tap do |hash|
            cell_array.each_with_index do |row, y|
              row.each_with_index do |cell_char, x|
                next if cell_char == '.'

                neighbors = if adjacent
                              find_neighboring_seats(cell_array, x, y, n_cols, n_rows)
                            else
                              find_visible_seats(cell_array, x, y, n_cols, n_rows)
                            end

                hash[[x, y]] = Seat.new(cell_char, neighbors)
              end
            end
          end
        end

        def find_neighboring_seats(cell_array, x, y, x_max, y_max)
          [].tap do |neighbors|

            ((x - 1)..(x + 1)).each do |x_tmp|
              next if x_tmp >= x_max

              ((y - 1)..(y + 1)).each do |y_tmp|
                next if y_tmp >= y_max
                next if cell_array[y_tmp][x_tmp] == '.'

                neighbors << [x_tmp, y_tmp]
              end
            end
          end
        end

        def find_visible_seats(cell_array, x, y, x_max, y_max)
          [].tap do |visible|
            DIRS.each do |offset|
              n = 1
              loop do
                x_tmp = x + n * offset[0]
                break if x_tmp >= x_max || x_tmp < 0
                y_tmp = y + n * offset[1]
                break if y_tmp >= y_max || y_tmp < 0

                unless cell_array[y_tmp][x_tmp] == '.'
                  visible << [x_tmp, y_tmp]
                  break
                end
                n += 1
              end
            end
          end
        end

        def more_than_x_neighbors_occupied?(seat_hash, neighbors, threshold)
          seat_hash.slice(*neighbors).count { |_, s| s.occupied? } >= threshold
        end

        def no_occupied_neighbors?(seat_hash, neighbors)
          seat_hash.slice(*neighbors).all? { |_, s| s.unoccupied? }
        end
      end

      class Seat
        attr_reader :neighbors

        def initialize(content, neighbors)
          @occupied = case content
                      when 'L'
                        false
                      when '#'
                        true
                      end
          @neighbors = neighbors
        end

        def occupied?
          occupied
        end

        def unoccupied?
          !occupied?
        end

        def occupy!
          @occupied = true
        end

        def empty!
          @occupied = false
        end

        private

        attr_reader :occupied
      end
    end
  end
end
