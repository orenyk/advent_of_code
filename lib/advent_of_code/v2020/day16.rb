module AdventOfCode
  module V2020
    class Day16
      def self.run(file, identify_fields = false)
        new(file).run(identify_fields)
      end

      def initialize(file)
        @rules = {}
        @my_ticket = []
        @nearby_tickets = []
        parse_note(file)
        @possible_rules = (1..my_ticket.length - 1).map { |v| [v, @rules.keys] }.to_h
      end

      def run(identify_fields)
        if identify_fields
          # reset
          @possible_rules = (0..my_ticket.length - 1).map { |v| [v, @rules.keys] }.to_h
          valid, _ = validate_tickets

          valid.each do |ticket|
            # check if all of the positions have a single possible rule
            break if possible_rules.values.all? { |ruleset| ruleset.length == 1 }

            # for each rule
            possible_rules.each do |position, possible_labels|
              # filter the possible labels based on whether they match the
              # ranges
              filtered_labels = possible_labels.select do |label|
                rule = rules[label]
                if rule[:range1].include?(ticket[position]) || rule[:range2].include?(ticket[position])
                  # puts "#{ticket[position]} matches #{label} - #{rule}"
                  true
                else
                  # puts "#{ticket[position]} does not match #{label} - #{rule}"
                  false
                end
              end

              @possible_rules[position] = filtered_labels

              # check if a rule uniquely belongs to a given position, in which
              # case remove it from the others. Need to do this iteratively
              if filtered_labels.length == 1
                filter_out_unique_rules
              end
            end
          end

          positions = possible_rules.transform_values { |ary| ary[0] }.invert
          positions.keys.select { |l| l.match?(/^departure.+/) }.reduce(1) do |acc, label|
            acc * my_ticket[positions[label]]
          end
        else
          _, invalid = validate_tickets
          invalid.flatten.sum
        end
      end

      private

      RULE_REGEX = /^(?<label>[a-z ]+): (?<min1>\d+)-(?<max1>\d+) or (?<min2>\d+)-(?<max2>\d+)$/

      attr_reader :rules, :my_ticket, :nearby_tickets, :invalid_values, :possible_rules

      def parse_note(file)
        lines = File.readlines(file).map(&:strip)
        parsing_rules = true
        parsing_my_ticket = false

        lines.each do |line|
          if parsing_rules
            if line.empty?
              parsing_rules = false
              parsing_my_ticket = true
              next
            else
              match = RULE_REGEX.match(line)
              @rules[match[:label]] = parse_rule(match)
            end
          elsif parsing_my_ticket
            if line.empty?
              parsing_my_ticket = false
              next
            elsif line == 'your ticket:'
              next
            else
              @my_ticket = parse_ticket(line)
            end
          else
            if line == 'nearby tickets:'
              next
            else
              @nearby_tickets << parse_ticket(line)
            end
          end
        end
      end

      def parse_rule(match)
        {
          range1: match[:min1].to_i..match[:max1].to_i,
          range2: match[:min2].to_i..match[:max2].to_i
        }
      end

      def parse_ticket(line)
        line.split(',').map(&:to_i)
      end

      def validate_tickets
        nearby_tickets.map do |ticket|
          ticket_tmp = ticket.clone
          rules.each do |_, rule|
            break if ticket_tmp.empty?
            ticket_tmp.reject! { |v| rule[:range1].include?(v) || rule[:range2].include?(v) }
          end
          ticket_tmp.empty? ? [ticket, []] : [ticket, ticket_tmp]
        end.partition { |ary| ary[1].empty? }.map do |part|
          part.map { |ary| ary[1].empty? ? ary[0] : ary[1] }
        end
      end

      def filter_out_unique_rules
        changed = false

        possible_rules.each do |pos, labels|
          if labels.length == 1 && possible_rules.values.count { |a| a.include? labels[0] } > 1
            changed = true
            possible_rules.keys.each do |other_pos|
              next if other_pos == pos
              @possible_rules[other_pos] -= labels
            end
          end
        end

        filter_out_unique_rules if changed
      end
    end
  end
end
