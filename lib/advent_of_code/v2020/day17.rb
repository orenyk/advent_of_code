module AdventOfCode
  module V2020
    class Day17
      def self.run(file, four_d = false)
        new(file, four_d).run
      end

      def initialize(file, four_d)
        @neighbors = {}
        @four_d = four_d
        # only track positives
        @initial_grid = parse_grid(file)
        @grid = initial_grid.clone
      end

      def run
        @grid = initial_grid.clone
        6.times do
          last_grid = grid.clone
          # go through all active cells and deactivate if necessary
          last_grid.each do |coords, _|
            next if [2, 3].include? neighbors[coords].intersection(last_grid.keys).count
            @grid.delete(coords)
          end

          # go through all active cell neighbors that weren't active cells and
          # activate if necessary
          (last_grid.keys.map { |k| neighbors[k] }.reduce([], &:+) - last_grid.keys).each do |coords|
            unless neighbors[coords]
              diffs = four_d ? NEIGHBOR_DIFFS_4D : NEIGHBOR_DIFFS_3D
              @neighbors[coords] = calculate_neighbors(coords, diffs)
            end
            if neighbors[coords].intersection(last_grid.keys).count == 3
              @grid[coords] = '#'
            end
          end
        end

        grid.count
      end

      private

      NEIGHBOR_DIFFS_3D = [].tap do |ary|
        (-1..1).each do |x|
          (-1..1).each do |y|
            (-1..1).each do |z|
              next if x.zero? && y.zero? && z.zero?
              ary << [x, y, z]
            end
          end
        end
      end

      NEIGHBOR_DIFFS_4D = [].tap do |ary|
        (-1..1).each do |x|
          (-1..1).each do |y|
            (-1..1).each do |z|
              (-1..1).each do |w|
                next if x.zero? && y.zero? && z.zero? && w.zero?
                ary << [x, y, z, w]
              end
            end
          end
        end
      end

      attr_reader :initial_grid, :grid, :neighbors, :four_d

      def parse_grid(file)
        Hash.new { '.' }.tap do |grid|
          File.readlines(file).each_with_index do |row, y|
            row.strip.split('').each_with_index do |cell, x|
              next if cell == '.'
              if four_d
                coords = [x, y, 0, 0]
              else
                coords = [x, y, 0]
              end
              grid[coords] = cell
              diffs = four_d ? NEIGHBOR_DIFFS_4D : NEIGHBOR_DIFFS_3D
              @neighbors[coords] = calculate_neighbors(coords, diffs)
            end
          end
        end
      end

      def calculate_neighbors(coords, diffs)
        diffs.map do |diff_array|
          coords.each_with_index.map { |c, i| c + diff_array[i] }
        end
      end
    end
  end
end
