module AdventOfCode
  module V2020
    class Day9
      def self.run(file, find_weakness = false)
        new(file).run(find_weakness)
      end

      def initialize(file)
        @nums = File.readlines(file).map { |l| l.strip.to_i }
        @current_pos = 25
      end

      def run(find_weakness)
        # reset
        @current_pos = 25

        no_sum = find_non_sum_value

        if find_weakness
          find_weakness_val(no_sum)
        else
          no_sum
        end
      end

      private

      WINDOW = 25

      attr_reader :nums, :current_pos

      def find_non_sum_value
        while current_pos < nums.length
          number = nums[current_pos]
          return number unless is_sum_of_pair_in_window(number, current_pos)
          @current_pos += 1
        end
        raise ArgumentError
      end

      def is_sum_of_pair_in_window(num, pos)
        possible_values = nums[(pos - WINDOW)..(pos - 1)].select { |v| v < num }
        is_sum_of_pair(num, possible_values)
      end

      def is_sum_of_pair(num, vals)
        smalls = vals.select { |v| v < num / 2.0 }
        larges = vals.select { |v| v > num / 2.0 }

        return false if smalls.empty? || larges.empty?

        smalls.each do |s|
          larges.each do |l|
            return true if s + l == num
          end
        end

        false
      end

      def find_weakness_val(number)
        start = 0
        width = 2
        vals = []

        loop do
          vals = nums[start..(start + width - 1)]
          break if vals.sum == number
          if vals.sum > number
            start += 1
            width = 2
          else
            width += 1
          end
        end

        vals.min + vals.max
      end
    end
  end
end
