module AdventOfCode
  module V2020
    class Day15
      def self.run(file, limit = '2020')
        new(file).run(limit.to_i)
      end

      def initialize(file)
        @numbers = File.read(file).strip.split(',').map(&:to_i)
        @number_hash = {}
        @last_number = nil
      end

      def run(limit)
        # reset
        @number_hash = Hash.new { [] }
        @last_number = nil

        numbers.each_with_index { |n, i| @number_hash[n] += [i + 1] }
        @last_number = numbers.last

        ((numbers.length + 1)..limit).each do |turn|
          if number_hash[last_number].length == 1
            number_hash[0].shift if number_hash[0].length > 1
            @number_hash[0] += [turn]
            @last_number = 0
          else
            diff = number_hash[last_number][1] - number_hash[last_number][0]
            number_hash[diff].shift if number_hash[diff].length > 1
            @number_hash[diff] += [turn]
            @last_number = diff
          end
        end

        last_number
      end

      private

      attr_reader :numbers, :number_hash, :last_number
    end
  end
end
