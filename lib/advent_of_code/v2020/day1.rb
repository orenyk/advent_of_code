module AdventOfCode
  module V2020
    class Day1
      def self.run(file, type)
        new(file).run(type)
      end

      def initialize(file)
        @file = file
        @smalls = []
        @larges = []
        @singles = []
        @pairs = {}
        @mean = DESIRED_SUM / 2
      end

      def run(type)
        case type
        when 'pair'
          find_pair
        when 'triple'
          find_triple
        else
          raise ArgumentError.new('Invalid type')
        end
      end

      private

      DESIRED_SUM = 2020

      attr_reader :file, :smalls, :larges, :singles, :pairs, :mean

      def find_pair
        File.open(file) do |f|
          f.each_line.lazy.each do |num_str|
            num = num_str.to_i
            product = if num < mean
                        @smalls << num
                        check_new_small
                      else
                        @larges << num 
                        check_new_large
                      end
            next if product.nil?

            return product
          end
        end
      end

      def check_new_small
        check_new(smalls.last, larges)
      end

      def check_new_large
        check_new(larges.last, smalls)
      end

      def check_new(num, array)
        array.each do |other_num|
          if num + other_num == DESIRED_SUM
            return num * other_num
          end
        end

        nil
      end

      def find_triple
        File.open(file) do |f|
          f.each_line.lazy.each do |num_str|
            num = num_str.to_i

            product = check_pairs(num)

            if product.nil?
              generate_new_pairs(num)
            else
              return product
            end
          end
        end
      end

      def check_pairs(num)
        pairs.each do |sum, product|
          return product * num if sum + num == DESIRED_SUM
        end

        nil
      end

      def generate_new_pairs(num)
        singles.each do |single|
          sum = single + num

          next unless sum < DESIRED_SUM

          if pairs[sum].nil?
            @pairs[sum] = num * single
          else
            # assume that the triple is unique (therefore any pair that sums to
            # the same value as another pair cannot be correct
            @pairs.delete(sum)
          end
        end

        @singles << num
      end
    end
  end
end
