module AdventOfCode
  module V2020
    class Day19
      def self.run(file)
        new(file).run
      end

      def initialize(file)
        @rules = {}
        parse_input(file)
        @rule_zero = /^#{generate_rule(0)}$/
      end

      def run
        messages.count { |msg| rule_zero.match?(msg) }
      end

      private

      attr_reader :rules, :messages, :rule_zero

      RULE_REGEX = /^(?<id>\d+): ("(?<char>[a-z])"|((?<single_rule>\d+)|(?<pair1a>\d+)( (?<pair1b>\d+))?( \| (?<pair2a>\d+)( (?<pair2b>\d+))?)?))$/

      def parse_input(file)
        rules_tmp, @messages = File.read(file).split("\n\n").map { |str| str.split("\n") }
        parse_rules(rules_tmp)
      end

      def parse_rules(rule_array)
        rule_array.each do |rule_str|
          match = RULE_REGEX.match(rule_str)

          if match.nil?
            raise ArgumentError, "oops"
          end

          if !match[:char].nil?
            @rules[match[:id].to_i] = match[:char]
          elsif !match[:single_rule].nil?
            @rules[match[:id].to_i] = [
              [match[:single_rule], nil]
            ]
          elsif !match[:pair2a].nil?
            @rules[match[:id].to_i] = [
              [match[:pair1a], match[:pair1b]],
              [match[:pair2a], match[:pair2b]]
            ]
          else
            @rules[match[:id].to_i] = [
              [match[:pair1a], match[:pair1b]]
            ]
          end
        end
      end

      def generate_rule(id)
        return "" if id.nil?
        return rules[id.to_i] unless rules[id.to_i].is_a? Array

        rule_str = rules[id.to_i].map do |rule_array|
          rule_array.map { |rule| generate_rule(rule) }.join
        end.join('|')

        "(#{rule_str})"
      end
    end
  end
end
