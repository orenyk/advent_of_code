module AdventOfCode
  module V2020
    class Day4
      def self.run(file, validate_values = false)
        new(file).run(validate_values)
      end

      def initialize(file)
        @passports = parse_passports(file)
      end

      def run(validate_values)
        passports.count { |p| valid?(p, validate_values) }
      end

      private

      REQUIRED_HEADERS = {
        'byr' => /^(19[2-9][0-9]|200[0-2])$/,
        'iyr' => /^20(1[0-9]|20)$/,
        'eyr' => /^20(2[0-9]|30)$/,
        'hgt' => /^(1([5-8][0-9]|9[0-3])cm|(59|6[0-9]|7[0-6])in)$/,
        'hcl' => /^#[0-9a-f]{6}$/,
        'ecl' => /^(amb|blu|brn|gry|grn|hzl|oth)$/,
        'pid' => /^[0-9]{9}$/
      }.freeze

      attr_reader :passports

      def parse_passports(file)
        File.read(file).split("\n\n").map do |p|
          p.split(/\s+/).map { |kv| kv.split(':') }.to_h
        end
      end

      def valid?(passport, validate_values)
        has_required_headers?(passport) &&
          (!validate_values || has_valid_values?(passport))
      end

      def has_required_headers?(passport)
        (REQUIRED_HEADERS.keys - passport.keys).empty?
      end

      def has_valid_values?(passport)
        REQUIRED_HEADERS.all? do |key, regex|
          regex.match(passport[key])
        end
      end
    end
  end
end
