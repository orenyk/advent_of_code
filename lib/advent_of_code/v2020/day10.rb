module AdventOfCode
  module V2020
    class Day10
      def self.run(file, find_combinations = false)
        new(file).run(find_combinations)
      end

      def initialize(file)
        @adapters = File.readlines(file).map { |l| l.strip.to_i }.sort
        @adapters.prepend(0)
        @adapters << adapters.max + 3
      end

      def run(find_combinations)
        if find_combinations
          calculate_all_combinations
        else
          find_distribution_and_multiply
        end
      end

      private

      attr_reader :adapters

      def find_distribution_and_multiply
        distribution = adapters[0..-2].each_with_index.map do |adapter, idx|
          adapters[idx + 1] - adapter
        end.tally
        distribution[1] * distribution[3]
      end

      def calculate_all_combinations
        # find all the possible adapters that can be connected to a single
        # adapter
        adapters.each_with_index.map do |adapter, idx|
          offset = 0
          possible_adapters = []

          loop do
            offset += 1
            break if idx + offset >= adapters.length || adapters[idx + offset] - adapter > 3
            possible_adapters << adapters[idx + offset]
          end

          [adapter, possible_adapters]
        # split each time you find an adapter that can only be connected to the
        # next adapter (e.g. connections that are always present)
        end.chunk_while do |arr1, arr2|
          arr1[1] != [arr2[0]]
        # multiply by the number of possibilities for the length of the variable
        # segments (didn't figure out the theory but I should)
        end.reduce(1) do |acc, arr|
          case arr.length
          when 1
            acc
          when 2
            2 * acc
          when 3
            4 * acc
          when 4
            7 * acc
          end
        end
      end
    end
  end
end
