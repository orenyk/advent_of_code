module AdventOfCode
  module V2022
    class Day2
      def self.run(file)
        new(file).run
      end

      def initialize(file)
        @file = file
      end

      def run
        current_score = 0

        File.open(file) do |f|
          f.each_line.lazy.each do |match|
            current_score += Match.new(*match.split(' ')).score
          end
        end

        current_score
      end

      attr_reader :file

      class Match

        def initialize(col_a, col_b)
          @col_a = COL_A[col_a]
          @col_b = COL_B[col_b]
        end

        def score
          PLAY_SCORE[col_b] + RESULT_SCORE[result]
        end

        private

        COL_A = {
          'A' => :rock,
          'B' => :paper,
          'C' => :scissors,
        }

        COL_B = {
          'X' => :rock,
          'Y' => :paper,
          'Z' => :scissors,
        }

        PLAY_SCORE = {
          rock: 1,
          paper: 2,
          scissors: 3,
        }

        RESULT_SCORE = {
          win: 6,
          draw: 3,
          lose: 0,
        }

        attr_reader :col_a, :col_b

        def result
          return :draw if col_a == col_b

          if (col_a == :rock && col_b == :paper) ||
              (col_a == :paper && col_b == :scissors) ||
              (col_a == :scissors && col_b == :rock)
            :win
          else
            :lose
          end
        end
      end
    end
  end
end
