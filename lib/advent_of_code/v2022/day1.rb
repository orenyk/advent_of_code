module AdventOfCode
  module V2022
    class Day1
      def self.run(file)
        new(file).run
      end

      def initialize(file)
        @file = file
        @cals = []
      end

      def run
        current_elf = 0
        max_total_cals = 0

        File.open(file) do |f|
          f.each_line.lazy.each do |calorie|
            if calorie.to_i.zero?
              max_total_cals = [max_total_cals, @cals[current_elf]].max
              current_elf += 1
              next
            else
              calorie = calorie.to_i
              if cals[current_elf].nil?
                @cals[current_elf] = calorie
              else
                @cals[current_elf] += calorie
              end
            end
          end
        end

        puts "max: #{max_total_cals}"
        puts "top 3: #{cals.sort.last(3).sum}"
      end

      private

      attr_reader :file, :cals
    end
  end
end
