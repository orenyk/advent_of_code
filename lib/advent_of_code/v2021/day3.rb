module AdventOfCode
  module V2021
    class Day3
      def self.run(file)
        new(file).run
      end

      def initialize(file)
        @file = file
        @length = 0
      end

      def run
        sums = Hash.new(0)
        File.open(file) do |f|
          f.each_line.lazy.each do |number|
            @length += 1
            # convert to array of bits and just sum up bits by position
            number.strip.reverse.chars.each_with_index do |bit, i|
              sums[i] += bit.to_i
            end
          end
        end

        max_bits = sums.keys.max
        # if the sum of bits in a given position is greater than half the length
        # of the report, that position is mostly 1's, otherwise mostly zeros
        gamma = (0..max_bits).map { |i| sums[i] >= length / 2 ? '1' : '0' }.join.reverse.to_i(2)
        # use a bitmask and XOR to invert gamma to get epsilon
        epsilon = ('1'*(max_bits+1)).to_i(2) ^ gamma

        epsilon * gamma
      end

      private

      attr_reader :file, :length
    end
  end
end
