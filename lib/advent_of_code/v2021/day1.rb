module AdventOfCode
  module V2021
    class Day1
      def self.run(file, window = false)
        new(file).run(window)
      end

      def initialize(file)
        @file = file
        @count = 0
      end

      def run(window)
        if window
          count_depths_with_window
        else
          count_depths
        end
      end

      private

      attr_reader :file, :count

      def count_depths
        last_depth = nil

        File.open(file) do |f|
          f.each_line.lazy.each do |depth|
            depth = depth.to_i
            if last_depth.nil?
              last_depth = depth
              next
            else
              @count += 1 if depth > last_depth
              last_depth = depth
            end
          end
        end

        count
      end

      def count_depths_with_window
        window = []

        File.open(file) do |f|
          f.each_line.lazy.each do |depth|
            depth = depth.to_i
            if window.length < 3
              window.push(depth)
              next
            else
              @count += 1 if window.shift < depth
              window.push(depth)
            end
          end
        end

        count
      end
    end
  end
end
