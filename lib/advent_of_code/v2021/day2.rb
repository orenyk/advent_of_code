module AdventOfCode
  module V2021
    class Day2
      def self.run(file, use_aim = false)
        new(file).run(use_aim)
      end

      def initialize(file)
        @file = file
        @position = 0
        @depth = 0
        @aim = 0
      end

      def run(use_aim)
        File.open(file) do |f|
          f.each_line.lazy.each do |instruction|
            match = /\A(?<dir>up|down||forward) (?<val>\d+)\Z/.match(instruction)
            case match[:dir]
            when 'up'
              if use_aim
                @aim -= match[:val].to_i
              else
                @depth -= match[:val].to_i
              end
            when 'down'
              if use_aim
                @aim += match[:val].to_i
              else
                @depth += match[:val].to_i
              end
            when 'forward'
              @position += match[:val].to_i
              @depth += match[:val].to_i * aim if use_aim
            else
              raise ArgumentError.new("Invalid command - #{instruction}")
            end
          end
        end

        position * depth
      end

      private

      attr_reader :file, :position, :depth, :aim
    end
  end
end
