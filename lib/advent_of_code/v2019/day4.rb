module AdventOfCode
  module V2019
    class Day4
      def self.run(min, max)
        min_digits = num_to_digits(min)
        max_digits = num_to_digits(max)

        (min_digits[0]..max_digits[0]).reduce(0) do |count, one|
          (one..9).each do |two|
            (two..9).each do |three|
              (three..9).each do |four|
                (four..9).each do |five|
                  (five..9).each do |six|
                    digits = [one, two, three, four, five, six]
                    num = digits_to_num(digits)
                    next if num < min || num > max || no_digit_pair?(digits)
                    count += 1
                  end
                end
              end
            end
          end
          count
        end
      end

      private

      def self.num_to_digits(number)
        number.to_s.chars.map(&:to_i)
      end

      def self.digits_to_num(digits)
        digits.join.to_i
      end

      def self.no_digit_pair?(digits)
        digits.uniq.map do |digit|
          idx = digits.find_index(digit)
          digits[idx] == digits[idx + 1] && digits[idx] != digits[idx + 2]
        end.none?
      end
    end
  end
end
