module AdventOfCode
  module V2019
    class Day2
      # You have to catch StandardError in Computer#run_program to account for
      # broken inputs
      def self.run(file, target)
        new(file, target).run
      end

      def initialize(file, target)
        @initial_memory = parse_memory(file)
        @target = target
        @noun = nil
        @verb = nil
      end

      def run
        nouns = (0..99).to_a
        verbs = (0..99).to_a

        nouns.each do |noun|
          verbs.each do |verb|
            memory_to_try = initial_memory.clone
            memory_to_try[1] = noun
            memory_to_try[2] = verb

            output = Computer.new(memory_to_try, []).run_program

            next unless output[:memory][0] == target

            return [noun, verb]

          rescue StandardError
            next
          end
        end
      end

      private

      attr_reader :initial_memory, :target, :noun, :verb

      def parse_memory(file)
        File.open(file) { |f| f.read }.split(",").map(&:to_i)
      end
    end
  end
end
