require "advent_of_code/v2019/computer/instruction"
require "advent_of_code/v2019/computer/add"
require "advent_of_code/v2019/computer/multiply"
require "advent_of_code/v2019/computer/input"
require "advent_of_code/v2019/computer/output"
require "advent_of_code/v2019/computer/jump_if_true"
require "advent_of_code/v2019/computer/jump_if_false"
require "advent_of_code/v2019/computer/less_than"
require "advent_of_code/v2019/computer/equals"
require "advent_of_code/v2019/computer/stop"

module AdventOfCode
  module V2019
    class Computer
      attr_reader :memory, :inputs, :outputs

      def initialize(initial_memory, inputs)
        @memory = initial_memory
        @pos = 0
        @inputs = inputs
        @outputs = []
        @stopped = false
      end

      def run_program
        iterate
        { outputs: outputs, memory: memory }
      end

      def stopped?
        stopped
      end

      private

      INSTRUCTIONS = {
        1 => Add,
        2 => Multiply,
        3 => Input,
        4 => Output,
        5 => JumpIfTrue,
        6 => JumpIfFalse,
        7 => LessThan,
        8 => Equals,
        99 => Stop
      }.freeze

      attr_reader :pos, :stopped

      def iterate
        instruction = parse_instruction
        if instruction.stop?
          @stopped = true
          return
        end
        @memory, @pos, @inputs, @outputs = instruction.execute
        return unless outputs.empty?
        iterate
      end

      def parse_instruction
        op = opcode(memory[pos])
        instruction = INSTRUCTIONS[op].new(memory, pos, inputs, outputs)
        instruction
      end

      # convert an instruction value with parameter modes to just the opcode
      def opcode(value)
        value % 100
      end
    end
  end
end
