module AdventOfCode
  module V2019
    class Day6
      CelestialObject = Struct.new(:visited, :distance)

      def self.run(file)
        # orbits is a hash with the keys representing each celestial object and
        # the value representing the object it orbits around
        orbits = File.readlines(file, chomp: true).reduce({}) do |h, o|
          base, object = o.split(")")
          h[object] = base
          h
        end
        new(orbits).run
      end

      def initialize(orbits)
        @orbits = orbits
        # non-edge neighbors of each non-edge object
        @neighbors = orbits.each_with_object(Hash.new([])) do |(obj, base), h|
          h[base] += [obj] if @orbits.values.include? obj
          h[obj] += [base] if @orbits.values.include? base
        end
        # all objects with at least one child
        @objects = orbits.values.reduce({}) do |h, obj|
          h[obj] = CelestialObject.new(false, Float::INFINITY)
          h
        end
        @start = "YOU"
        @finish = "SAN"
      end

      def run
        min_transfer(start, finish)
      end

      private

      attr_reader :orbits, :neighbors, :objects, :start, :finish

      # Dijkstra's algorithm
      def min_transfer(start, finish)
        starting_object = orbits[start]
        @objects[starting_object].visited = true
        @objects[starting_object].distance = 0
        iterate(starting_object)
      end

      def iterate(object)
        next_objects = neighbors[object]
        current_distance = objects[object].distance
        return current_distance + 1 if next_objects.include? orbits[finish]

        (next_objects & objects.keys).each do |obj|
          @objects[obj].distance = current_distance + 1
        end

        @objects.delete(object)
        iterate(next_object)
      end

      def next_object
        objects.min_by { |_, v| v.distance }[0]
      end
    end
  end
end
