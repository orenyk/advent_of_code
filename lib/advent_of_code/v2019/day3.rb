module AdventOfCode
  module V2019
    class Day3
      def self.run(file)
        wire1, wire2 = File.readlines(file, chomp: true).map do |w|
          w.split(",")
        end
        new(wire1, wire2).run
      end

      def initialize(wire1, wire2)
        @wire1 = points_from_wire(wire1)
        @wire2 = points_from_wire(wire2)
        @intersections = []
      end

      def run
        intersections = wire1 & wire2
        intersections.map { |pt| point_score(pt) }.min
      end

      private

      GENERATORS = {
        "U" => [0, 1],
        "R" => [1, 0],
        "D" => [0, -1],
        "L" => [-1, 0]
      }.freeze

      attr_reader :wire1, :wire2, :intersections

      def points_from_wire(links)
        links.reduce([[0, 0]]) do |pts, definition|
          pts + points_from_link(pts.last, definition)
        end.drop(1)
      end

      def points_from_link(base, definition)
        direction = definition[0]
        length = definition[1..-1].to_i
        dx = GENERATORS[direction][0]
        dy = GENERATORS[direction][1]

        Array.new(length) do |n|
          [base[0] + dx * (n + 1), base[1] + dy * (n + 1)]
        end
      end

      def point_score(point)
        # Add 2 since we remove the base element on line 36
        wire1.find_index(point) + wire2.find_index(point) + 2
      end
    end
  end
end
