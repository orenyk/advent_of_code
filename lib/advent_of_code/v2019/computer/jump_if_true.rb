module AdventOfCode
  module V2019
    class Computer
      class JumpIfTrue < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          new_position = parameter(1).zero? ? position + length : parameter(2)
          [memory, new_position, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "JumpIfTrue must have an opcode of 5 with two parameters".freeze

        def length
          3
        end

        def valid?
          opcode == 5 && parameters_present?
        end
      end
    end
  end
end
