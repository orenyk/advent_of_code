module AdventOfCode
  module V2019
    class Computer
      class Stop < Instruction
        def initialize(memory, position, _inputs, _outputs)
          @opcode = parse_opcode(memory[position])
          super
        end

        def length
          1
        end

        def stop?
          true
        end

        private

        INVALID_INSTRUCTION = "Stop must have an opcode of 99".freeze

        def valid?
          opcode == 99
        end
      end
    end
  end
end
