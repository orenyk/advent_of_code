module AdventOfCode
  module V2019
    class Computer
      class Output < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          puts parameter(1)
          outputs.push(parameter(1))
          [memory, position + length, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "Output must have an opcode of 4 with three parameters".freeze

        def length
          2
        end

        def valid?
          opcode == 4 && parameters_present?
        end
      end
    end
  end
end
