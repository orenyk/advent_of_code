module AdventOfCode
  module V2019
    class Computer
      class Add < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          @memory[raw_parameter(3)] = parameter(1) + parameter(2)
          [memory, position + length, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "Add must have an opcode of 1 with three parameters".freeze

        def length
          4
        end

        def valid?
          opcode == 1 && parameters_present? && !immediate_parameter?(3)
        end
      end
    end
  end
end
