module AdventOfCode
  module V2019
    class Computer
      class Input < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          @memory[raw_parameter(1)] = inputs.shift
          [memory, position + length, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "Input must have an opcode of 3 with three parameters and at least one available input".freeze

        def length
          2
        end

        def valid?
          opcode == 3 && parameters_present? && !immediate_parameter?(1) && !inputs.empty?
        end
      end
    end
  end
end
