module AdventOfCode
  module V2019
    class Computer
      class JumpIfFalse < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          new_position = parameter(1).zero? ? parameter(2) : position + length
          [memory, new_position, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "JumpIfFalse must have an opcode of 6 with two parameters".freeze

        def length
          3
        end

        def valid?
          opcode == 6 && parameters_present?
        end
      end
    end
  end
end

