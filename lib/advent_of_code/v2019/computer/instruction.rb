module AdventOfCode
  module V2019
    class Computer
      class Instruction
        def initialize(_memory, _position, _inputs, _outputs)
          raise ArgumentError.new(self.class::INVALID_INSTRUCTION) unless valid?
        end

        def stop?
          false
        end

        private

        attr_reader :memory, :position, :inputs, :outputs, :opcode, :parameters

        def save_inputs(memory, position, inputs, outputs)
          @memory = memory.clone
          @position = position
          @inputs = inputs
          @outputs = outputs
        end

        def parse_opcode(value)
          value % 100
        end

        def parse_parameters(memory, position)
          Array.new(length - 1) do |i|
            first_value = memory[position]
            is_immediate = ((first_value / (100 * (10 ** i))) % 10) == 1
            [memory[position + i + 1], is_immediate]
          end
        end

        def parameter(idx)
          raise ArgumentError.new("Invalid index") if idx >= length

          parameter_bundle = parameters[idx - 1]
          if parameter_bundle[1]
            parameter_bundle[0]
          else
            memory[parameter_bundle[0]]
          end
        end

        def raw_parameter(idx)
          raise ArgumentError.new("Invalid index") if idx >= length
          parameters[idx - 1][0]
        end

        def immediate_parameter?(idx)
          raise ArgumentError.new("Invalid index") if idx >= length
          parameters[idx - 1][1]
        end

        def parameters_present?
          parameters.map { |bundle| bundle[0] }.all? { |val| val.is_a? Integer }
        end
      end
    end
  end
end
