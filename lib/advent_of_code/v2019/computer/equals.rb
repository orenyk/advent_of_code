module AdventOfCode
  module V2019
    class Computer
      class Equals < Instruction
        def initialize(memory, position, inputs, outputs)
          save_inputs(memory, position, inputs, outputs)
          @opcode = parse_opcode(memory[position])
          @parameters = parse_parameters(memory, position)
          super
        end

        def execute
          value = parameter(1) == parameter(2) ? 1 : 0
          @memory[raw_parameter(3)] = value
          [memory, position + length, inputs, outputs]
        end

        private

        INVALID_INSTRUCTION = "Equals must have an opcode of 8 with three parameters".freeze

        def length
          4
        end

        def valid?
          opcode == 8 && parameters_present? && !immediate_parameter?(3)
        end
      end
    end
  end
end
