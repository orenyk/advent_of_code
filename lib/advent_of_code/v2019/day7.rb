module AdventOfCode
  module V2019
    class Day7
      PHASE_RANGE = (5..9).to_a.freeze

      def self.run(file)
        new(file).run
      end

      def initialize(file)
        @program = parse_memory(file)
        @max_thruster = 0
        @last_thruster = 0
      end

      def run
        PHASE_RANGE.permutation.each do |phase_array|
          computers = phase_array.map do |phase_setting|
            Computer.new(program.clone, [phase_setting])
          end
          computers.first.inputs.push(0)
          iterate(computers, 0)
          @max_thruster = last_thruster if last_thruster > max_thruster
          @last_thruster = 0
        end
        max_thruster
      end

      private

      attr_reader :program, :max_thruster, :current_amplifier, :last_thruster

      def parse_memory(file)
        File.open(file) { |f| f.read }.split(",").map(&:to_i)
      end

      def iterate(computers, current)
        current_amplifier = computers[current]
        current_amplifier.run_program
        current_output = current_amplifier.outputs.pop

        if current == computers.length - 1
          return if current_amplifier.stopped?
          @last_thruster = current_output
        end

        next_idx = current == computers.length - 1 ? 0 : current + 1
        computers[next_idx].inputs.push(current_output)
        iterate(computers, next_idx)
      end
    end
  end
end
