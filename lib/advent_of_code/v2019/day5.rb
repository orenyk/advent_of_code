require "advent_of_code/v2019/computer"

module AdventOfCode
  module V2019
    class Day5
      def self.run(file, input)
        new(file, input).run
      end

      def initialize(file, input)
        @initial_memory = parse_memory(file)
        @input = input
      end

      def run
        Computer.new(initial_memory, [input]).run_program
      end

      private

      attr_reader :initial_memory, :input

      def parse_memory(file)
        File.open(file) { |f| f.read }.split(",").map(&:to_i)
      end
    end
  end
end
