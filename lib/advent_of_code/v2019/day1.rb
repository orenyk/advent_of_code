module AdventOfCode
  module V2019
    class Day1

      def self.run(file)
        File.open(file) do |f|
          f.each_line.lazy.reduce(0) do |sum, mass|
            sum + self.new(mass.to_i).calculate_fuel
          end
        end
      end

      def initialize(mass)
        @mass = mass
      end

      def calculate_fuel
        initial_fuel = required_fuel(mass)
        total_fuel(initial_fuel)
      end

      private

      attr_reader :mass

      def required_fuel(fuel)
        [0, (fuel / 3).floor - 2].max
      end

      def total_fuel(fuel)
        new_fuel = required_fuel(fuel)
        return fuel if new_fuel.zero?

        fuel + total_fuel(new_fuel)
      end
    end
  end
end
